# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class RegistryConfig(AppConfig):
    label = 'main'
    name = 'main'
    verbose_name = _('Main app')

    def ready(self):  # pragma: no cover
        from . import receivers  # noqa

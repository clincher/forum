from django.conf import settings
from django.core.mail import send_mail
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.template.loader import render_to_string


@receiver(post_save, sender='forum_conversation.Post', weak=True)
def new_message_in_followed_topic(**kwargs):
    post = kwargs['instance']
    emails_list = post.topic.subscribers.values_list('email', flat=True)

    for email in emails_list:
        send_mail(
            subject='New post in the topic you follow',
            message=render_to_string(
                'emails/new_message_in_followed_topic.html',
                {'post': post}
            ),
            from_email=settings.DEFAULT_FROM_EMAIL,
            recipient_list=[email],
            html_message=render_to_string(
                'emails/new_message_in_followed_topic.html',
                {'post': post}
            )
        )

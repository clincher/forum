from custom_user.models import AbstractEmailUser


class EmailUser(AbstractEmailUser):
    """
    Without that class django-machina complained that User doesn't have username
    """
    class Meta(AbstractEmailUser.Meta):  # noqa: D101
        swappable = 'AUTH_USER_MODEL'

    @property
    def username(self):
        return getattr(self, self.USERNAME_FIELD)

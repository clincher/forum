"""App configuration for custom_user."""
from django.apps import AppConfig


class UsersConfig(AppConfig):

    """Default configuration for custom_user."""

    name = 'users'
    verbose_name = "Forum User"

from django.contrib import admin
from custom_user.admin import EmailUserAdmin
from .models import EmailUser


admin.site.register(EmailUser, EmailUserAdmin)
